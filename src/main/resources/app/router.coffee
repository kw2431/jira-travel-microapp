define ->
  
  class Router extends Backbone.Router
    routes:
      ''          : 'addTrip'
      'trips'     : 'myTrips'
      'trip/book' : 'bookTrip'
      'trip/new'  : 'addTrip'
      '*path'     : 'notFound'

    initialize: (options)->
      @addTripView = options.addTripView

    before: ->
      $('.flash .alert').remove()

    activate: (id) ->
      $('.content').addClass 'hidden'
      $('#'+id).removeClass 'hidden'
      $('.topnav').removeClass 'active'
      $('.'+id).addClass 'active'
      return @

    addTrip: =>
      @activate 'addtrip'
      @addTripView.focus()

    myTrips: ->
      @activate 'mytrips'

    bookTrip: ->
      @activate 'booktrip'

    notFound: ->
      @activate '404'