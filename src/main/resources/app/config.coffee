# JIRA Travel Approval microapp
# =============================
# Before you can use this, you'll have to perform the following steps in your JIRA instance:
#
#     1. Create a new project to store your travel requests. The default project key used by this app is "TRAVEL"
#     2. Create 5 new custom fields:
#       1. Origin (string)
#       2. Destination (string)
#       3. Depart Date (date)
#       4. Return Date (date)
#       5. Purchase Amount (float)
#       6. Currency (string)
#     3. Create a issue type called "Travel Request"
#     4. Create a custom workflow with four new statuses:
#       1. Open
#       2. Approved
#       3. Denied
#       4. Booked
#
# How you manage the transitions between those four statuses is up to you. You can modify any additional
# configurations in the object below.

define

  # Specify your JIRA config here:
  restPath:           '/jira/rest' # This is the relative path to the rest api (http://localhost[/jira/rest]/api/2/...
  projectKey:         'TRAVEL' # This is the project key where travel approvals will be created under
  issueTypeName:      'Travel Request' # Name of the Issue Type you want issues to be created with
  bookTransitionName: 'Book' # Name of the transition you want to use to "book" travel requests
  customFields: # These are the names of your custom fields. We'll match them with the actual custom field at runtime
    origin:
      name: "Origin"
    destination:
      name: "Destination"
    departDate:
      name: "Depart Date"
    returnDate:
      name: "Return Date"
    purchaseAmt:
      name: "Purchase Amount"
    currency:
      name: "Currency"