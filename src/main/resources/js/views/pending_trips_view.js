// Generated by CoffeeScript 1.3.3
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(function() {
  var PendingTripsView;
  return PendingTripsView = (function(_super) {

    __extends(PendingTripsView, _super);

    function PendingTripsView() {
      return PendingTripsView.__super__.constructor.apply(this, arguments);
    }

    PendingTripsView.prototype.el = $('#pendingtrips-rows');

    PendingTripsView.prototype.template = _.template($('#pendingtrips-template').html());

    PendingTripsView.prototype.initialize = function(options) {
      return this.render();
    };

    PendingTripsView.prototype.events = {
      "click .complete-booking": "completeBooking"
    };

    PendingTripsView.prototype.completeBooking = function(e) {
      var el, trip,
        _this = this;
      el = $(e.currentTarget);
      trip = el.data();
      $.get("" + config.restPath + "/api/2/issue/" + trip.key + "/transitions", function(d) {
        var bookTransition;
        bookTransition = _.find(d.transitions, function(t) {
          return t.name === config.bookTransitionName;
        });
        return $.ajax({
          url: "" + config.restPath + "/api/2/issue/" + trip.key + "/transitions",
          type: "post",
          dataType: "json",
          contentType: "application/json",
          processData: false,
          data: JSON.stringify({
            transition: {
              id: bookTransition.id
            }
          }),
          success: function() {
            el.removeClass('btn-success').addClass('btn-inverse').addClass('disabled').html('Booked');
            _this.collection.get(trip.id).set('status', {
              name: "Booked"
            }, {
              silent: true
            });
            return _this.collection.trigger('change');
          }
        });
      });
      return false;
    };

    PendingTripsView.prototype.render = function() {
      var _this = this;
      return this.collection.filter(function(d) {
        return d.attributes.status.name === "Approved";
      }).sort(function(a, b) {
        return a.get(config.customFields.departDate.id) > b.get(config.customFields.returnDate.id);
      }).each(function(model) {
        return _this.$el.append(_this.template(model));
      });
    };

    return PendingTripsView;

  })(Backbone.View);
});
